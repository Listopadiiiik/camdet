import cv2 as cv
import numpy as np
import urllib.request


image_path = 'horse.jpeg'
model_path = 'MobileNetSSD_deploy.caffemodel'
prototxt_path = 'MobileNetSSD_deploy.prototxt'
min_confidence = 0.2

classes = ["background", "aeroplan", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

color = np.random.uniform(0, 255, size=(len(classes), 3))

np.random.seed(543210)
net = cv.dnn.readNetFromCaffe(prototxt_path, model_path)


res = cv.VideoWriter('video.avi', -1, 20.0, (1920, 1080))
cap = cv.VideoCapture('http://***************/video')
while True:
    _, image = cap.read()
    if _ == True:
        frame = cv.flip(image, 0)
        res.write(image)
    height, width = image.shape[0], image.shape[1]
    bobl = cv.dnn.blobFromImage(cv.resize(image, (300, 300)), 0.01, (300, 300), (130, 1))
    net.setInput(bobl)
    detected_objects = net.forward()


    for i in range(detected_objects.shape[2]):
        confidence = detected_objects[0][0][i][2]
        if confidence > min_confidence:

            index = int(detected_objects[0, 0, i, 1])
            upper_left_x = int(detected_objects[0, 0, i, 3] * width)
            upper_left_y = int(detected_objects[0, 0, i, 4] * height)
            lower_right_x = int(detected_objects[0, 0, i, 5] * width)
            lower_right_y = int(detected_objects[0, 0, i, 6] * height)

            prediction_text = f'{classes[index]}: {confidence:.2}'
            cv.rectangle(image, (upper_left_x, upper_left_y), (lower_right_x, lower_right_y), color[index], 3)
            cv.putText(image, prediction_text, (upper_left_x, upper_left_y -
            15 if upper_left_y > 30 else upper_left_y + 15),
            cv.FONT_HERSHEY_SIMPLEX, 0.6, color[index], 2)


    cv.imshow('Detected objects', image)
    cv.waitKey(5)
    print(cap)
cv.destroyAllWindows()
cap.release()


















